﻿using System;
using MongoDB.Driver;
using NoSql.Impl;

namespace NoSql
{
    /* Это основной метод программы. В общем-то я поленился делать даже интерфейс в виде "введите число операции 1: прочитать... 2. вставить...
     * Поэтому просто в коде комментариями сделал.
     * Все они работают. Прочитать все, делал просто для отладки, то есть именно для добавления\изменения и т.п. они не нужны абсолюнто.
     * Написал не все функции, потому что они все по аналогии делаются.
     */
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "mongodb://localhost:27017";
            MongoClient client = new MongoClient(connectionString);
            IMongoDatabase database = client.GetDatabase("test");
            /*GetCollectionsNames(client).GetAwaiter();

            BsonClassMap.RegisterClassMap<User>(cm =>
            {
                cm.AutoMap();
            });
            //Console.ReadLine();

            var collection = database.GetCollection<BsonDocument>("User");
            BsonDocument person1 = new BsonDocument
            {
                { "LastName","1" },
                { "FirstName","2"},
                {"MiddleName","3"}
            };
            collection.InsertOneAsync(person1).GetAwaiter().GetResult();*/
            

            //Пример добавления класса.
            //ClassRepository.Create(new Class() { Name = " 5 Б", TeacherId = "1", Students = new List<string> { "2", "3", "4" } }, database);

            var a = ClassRepository.ReadAll(database);

            //Пример удаления класса.
            //ClassRepository.Destroy(a[0].Id, database);
            
            var b = ClassRepository.Read(a[0].Id, database);
            
            
            //var c = new Class() { Id = b.Id, Name = "6 Б", Students = b.Students, TeacherId = b.TeacherId };

            //Пример изменения класса.
            //ClassRepository.Update(b, c, database);

            //var d = ClassRepository.Read(a[0].Id, database);

            Console.ReadLine();
        }
    }
}
