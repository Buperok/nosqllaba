﻿using MongoDB.Bson;
using MongoDB.Driver;
using NoSql.Domain;
using System.Collections.Generic;
using System.Linq;

/* Что здесь происходит?
 * Есть особый класс IMongoDatabase из MongoDB.Driver. Короче, суть в том, что при подключении в таблице проверяется целостность структуры. 
 * Если изменить класс(добавить поле\удалить\изменить), то Monga "сама" подправит соответствующую таблицу, если сможет.
 * Поэтому не нужно как-то создавать таблицы, нужно только создать саму базу.
 * 
 * Для простоты я во все методы прокидываю вот это подключение к базе IMongoDatabase, по уму, его нужно либо тут открывать, либо как-то накапливать несколько изменений, а потом одной транзакцией их производить и т.д.
 * 
 * database.GetCollection<T>("название таблицы") - открывает таблицу и пытается смапить то что там лежит на класс T. Если не удаётся - Exception(вроде).
 * Для выбора элемента используется фильтр, если он пуст, значит ВСЕ записи.
 */

namespace NoSql.Impl
{
    public static class ClassRepository
    {
        public static void Create(Class data, IMongoDatabase database)
        {
            database.GetCollection<Class>("Class").InsertOne(data);
        }

        public static void Destroy(string id, IMongoDatabase database)
        {
            var filter = Builders<Class>.Filter.Eq("Id", id);
            database.GetCollection<Class>("Class").DeleteOne(filter);
        }

        public static List<string> GetListFullNameStudents(string id, IMongoDatabase database)
        {
            var studentsList = Read(id, database).Students;
            var filter = Builders<User>.Filter.In("Id", studentsList);
            var userList = database.GetCollection<User>("User").Find(filter).ToList();

            var result = new List<string>();
            foreach(var i in userList)
            {
                result.Add(i.FirstName + " " + i.LastName + " " + i.MiddleName);
            }

            return result;
        }

        public static Class Read(string id, IMongoDatabase database)
        {
            var filter = Builders<Class>.Filter.Eq("Id", id);
            return database.GetCollection<Class>("Class").Find(filter).FirstOrDefault();
        }

        public static List<Class> ReadAll(IMongoDatabase database)
        {
            var filter = new BsonDocument();
            return database.GetCollection<Class>("Class").Find(filter).ToList();
        }

        public static void Update(Class data1, Class data2, IMongoDatabase database)
        {
            database.GetCollection<Class>("Class").ReplaceOne(data1.ToBsonDocument(), data2);
        }
    }
}
