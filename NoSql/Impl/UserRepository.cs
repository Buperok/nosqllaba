﻿using MongoDB.Driver;
using NoSql.Domain;
using System;
using System.Collections.Generic;

namespace NoSql.Impl
{
    public class UserRepository
    {
        public void Create(User data, IMongoDatabase database)
        {
            database.GetCollection<User>("User").InsertOne(data);
        }

        public void Destroy(string id, IMongoDatabase database)
        {
            var filter = Builders<User>.Filter.Eq("Id", id);
            database.GetCollection<User>("User").DeleteOne(filter);
        }

        public bool IsTeacher(string id)
        {
            throw new NotImplementedException();
        }

        public User Read(string id, IMongoDatabase database)
        {
            throw new NotImplementedException();
        }

        public List<User> ReadAll(IMongoDatabase database)
        {
            throw new NotImplementedException();
        }

        public void Update(User data1, User data2, IMongoDatabase database)
        {
            throw new NotImplementedException();
        }
    }
}
