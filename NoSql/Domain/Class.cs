﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NoSql.Domain
{
    /// <summary>
    /// Школьный класс.
    /// </summary>
    public class Class
    {
        /// <summary>
        /// ID класса.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// Название класса.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ID классного руководителя.
        /// </summary>
        public string TeacherId { get; set; }

        /// <summary>
        /// Список учащихса класса.
        /// </summary>
        public List<string> Students { get; set; }
    }
}
