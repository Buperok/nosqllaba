﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NoSql.Domain
{
    /// <summary>
    /// Школа.
    /// </summary>
    public class School
    {
        /// <summary>
        /// ID школы.
        /// </summary>
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        string Id { get; set; }

        /// <summary>
        /// Название школы.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// ID директора.
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        string DirectorId { get; set; }
    }
}
